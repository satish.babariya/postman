import resty, {
  Controller,
  Get,
  Context,
  Service,
  Inject,
  Param,
  Post,
  Body,
  Put,
  Delete,
} from "@restyjs/core";

@Service()
class BeanFactory {
  create() {
    return "BeanFactory";
  }
}

@Controller("/hello")
class HelloController {
  @Inject() beanFactory!: BeanFactory;

  @Get("/")
  index() {
    return "Hello World from " + this.beanFactory.create();
  }

  @Get("/health")
  health(ctx: Context) {
    return ctx.res.json({ status: "ok" }).status(200);
  }
}

@Controller("/users")
export class UserController {
  @Get("/")
  getAll() {
    return "This action returns all users";
  }

  @Get("/:id")
  getOne(@Param("id") id: number) {
    return "This action returns user #" + id;
  }

  @Post("/")
  post(@Body() user: any) {
    return "Saving user...";
  }

  @Put("/:id")
  put(@Param("id") id: number, @Body() user: any) {
    return "Updating a user...";
  }

  @Delete("/:id")
  remove(@Param("id") id: number) {
    return "Removing user...";
  }
}

const app = resty({
  routePrefix: "/api",
  controllers: [HelloController, UserController],
  providers: [],
});
