import yargs from "yargs";
import chalk from "chalk";
import fs from "fs";
import path from "path";
import { generate } from "./generate";

const package_json_path = process.cwd() + "/package.json";

if (fs.existsSync(package_json_path) == false) {
  console.log(
    chalk.red(
      `please run this command in dir your projects package.json file is`
    ),
    path
  );
  process.exit(0);
}

const options = yargs
  .usage("Usage: [main.ts file path]")
  .option("p", {
    alias: "path",
    describe: "main.ts file path",
    type: "string",
    default: process.cwd() + "/src/main.ts",
    demandOption: true,
  })
  .option("o", {
    alias: "output",
    describe: "collection.json file path",
    type: "string",
    default: process.cwd() + "/collection.json",
    demandOption: true,
  })
  .option("e", {
    alias: "endpoint",
    describe: "http url endpoint",
    type: "string",
    default: "http://localhost:8080",
    demandOption: true,
  }).argv;

const file = options.path as string;
const endpoint = options.endpoint as string;
const outputfile = options.output as string;

if (fs.existsSync(file)) {
  (async () => {
    try {
      await generate(
        package_json_path,
        endpoint,
        outputfile,
        path.resolve(file).toString()
      );
      process.exit(0);
    } catch (e) {
      console.log(chalk.red(e));
      process.exit(0);
    }
  })();
} else {
  console.log(chalk.red(`file not found at`), file);
  process.exit(0);
}
