import {
  Container,
  ControllerMetadata,
  HTTPMethodMetadata,
} from "@restyjs/core";
import { MetadataKeys } from "@restyjs/core/metadataKeys";
import { Collection, Item } from "postman-collection";
import fs from "fs";
import chalk from "chalk";

async function generate(
  package_json_path: string,
  endpoint: string,
  outputfile: string,
  path: string
) {
  await import(path);
  const package_json: any = await import(package_json_path);

  var collection = new Collection();

  collection.name = package_json.name;
  collection.description = package_json.description;
  collection.version = package_json.version;

  const application: any = Container.get("resty:application");

  var requestURL = endpoint;

  if (application.routePrefix) {
    requestURL = requestURL + application.routePrefix;
  }

  application.controllers.map((controller: any) => {
    const controllerMetadata: ControllerMetadata = Reflect.getMetadata(
      MetadataKeys.controller,
      controller
    );

    const controllerPath = requestURL + controllerMetadata.path;

    if (controllerMetadata == null) {
      throw Error(`${controller.name} metadata not found`);
    }

    const arrHttpMethodMetada: HTTPMethodMetadata[] =
      Reflect.getMetadata(MetadataKeys.httpMethod, controller) ?? [];

    arrHttpMethodMetada.map((mehtodMetadata) => {
      collection.items.add(
        new Item({
          name: controllerMetadata.path + mehtodMetadata.path,
          request: {
            url: controllerPath + mehtodMetadata.path,
            method: mehtodMetadata.method.toString(),
          },
        })
      );
    });
  });

  await new Promise((resolve, reject) => {
    fs.writeFile(outputfile, JSON.stringify(collection.toJSON()), (err) => {
      if (err) {
        reject(err);
      } else {
        console.log(chalk.green(`Sucessfully Written`), outputfile);
        resolve();
      }
    });
  });

  //   console.log(chalk.green(`Sucessfully Written`), outputfile);
  return;
}

export { generate };
