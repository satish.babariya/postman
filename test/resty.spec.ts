import { generate } from "../src/generate";

describe("postman-collection-generator", () => {
  it("test postman-collection generator", async (done) => {
    const package_json_path: string = process.cwd() + "/package.json";
    const endpoint: string = "http://localhost:8080";
    const outputfile: string = process.cwd() + "/collection.json";
    const path: string = process.cwd() + "/example/main.ts";
    await generate(package_json_path, endpoint, outputfile, path);
    done();
  });
});
